#!/usr/bin/python3
#
# Name: sns-ms-ip.py
# Description: Retrieves all IPv4, IPv6 and URL addresses from
# https://docs.microsoft.com/en-us/office365/enterprise/urls-and-ip-address-ranges
# and output a CSV file that can be imported into the Stormshield Network
# Security Appliance
# Created by: Luke Michael "sludge3000" Savage
#


# Import required modules
import urllib.request
import re
import ipaddress


# Read page bytes and convert to UTF8
page = urllib.request.urlopen("https://docs.microsoft.com/en-us/office365/enterprise/urls-and-ip-address-ranges")
pagebytes = page.read()
pagestr = pagebytes.decode("utf8")
page.close()


# RegEx magic
# IPv4 hosts without CIDR mask
ipv4hostlist = re.findall(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3},|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\/32',pagestr)
# IPv4 networks with CIDR masks
ipv4networklist = re.findall(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/(?!32)\d{1,2})',pagestr)
# IPv6 hosts
ipv6hostlist = re.findall(r'([0-9a-f]+:[0-9a-f]+:[0-9a-f]*:[0-9a-f:]*)\/128',pagestr)
# IPv6 networkS
ipv6networklist = re.findall(r'([0-9a-f]+:[0-9a-f]+:[0-9a-f]*:[0-9a-f:]*\/(?!128)\d+)',pagestr)


# Sort and order
uniqipv4hosts = set(ipv4hostlist)
orderipv4hosts = sorted(uniqipv4hosts)
uniqipv4nets = set(ipv4networklist)
orderipv4nets = sorted(uniqipv4nets)
uniqipv6hosts = set(ipv6hostlist)
orderipv6hosts = sorted(uniqipv6hosts)
uniqipv6nets = set(ipv6networklist)
orderipv6nets = sorted(uniqipv6nets)

ipv4group = ''
ipv6group = ''

with open('IMPORT_ME.csv', 'w+', newline='') as outfile:
	counterv4hosts = 0
	counterv4nets = 0
	counterv6hosts = 0
	counterv6nets = 0
	for ip in orderipv4hosts:
		counterv4hosts += 1
		outfile.write("host,MicrosoftHost_" + str(counterv4hosts) + "," + ip + ',,static,,""\n')
		ipv4group = ipv4group + 'MicrosoftHost_' + str(counterv4hosts) + ','
	for ip in orderipv6hosts:
		counterv6hosts += 1
		outfile.write("host,MicrosoftIPv6Host_" + str(counterv6hosts) + ",," + ip + ',static,,""\n')
		ipv6group = ipv6group + 'MicrosoftIPv6Host_' + str(counterv6hosts) + ','
	for ip in orderipv4nets:
		counterv4nets += 1
		network = ipaddress.ip_network(ip).network_address
		netmask = ipaddress.ip_network(ip).netmask
		prefixlen = ipaddress.ip_network(ip).prefixlen
		outfile.write("network,MicrosoftNet_" + str(counterv4nets) + "," + str(network) + "," + str(netmask) + "," + str(prefixlen) + ",,," + '""\n')
		ipv4group = ipv4group + 'MicrosoftNet_' + str(counterv4nets) + ','
	for ip in orderipv6nets:
		counterv6nets += 1
		network = ipaddress.ip_network(ip).network_address
		prefixlen = ipaddress.ip_network(ip).prefixlen
		outfile.write("network,MicrosoftIPv6Net_" + str(counterv6nets) + ",,,," + str(network) + "," + str(prefixlen) + ',""\n')
		ipv6group = ipv6group + 'MicrosoftIPv6Net_' + str(counterv6nets) + ','
	outfile.write('group,Grp_Microsoft_IPv4,"' + ipv4group + '",""\n')
	outfile.write('group,Grp_Microsoft_IPv6,"' + ipv6group + '",""\n')


# URL handler
# Initialize lists and strings
urllist1 = []
urllist2 = []
urllist3 = []
urllist4 = []
urllist5 = []
urlstring1 = ''
urlstring2 = ''


# All contents between <code> tags (the IPs and URL we want)
# This is required to filter out URLs hidden in the HTML code that we do not want
urllist1 = re.findall(r'<code>([a-zA-Z0-9.,*\s]+)</code>',pagestr)

# Concatenate all results with formatting
for i in urllist1:
	urlstring1 += ', ' + i

# Reformat
urlstring2 = urlstring1.replace(', ', '\n')

# Match all URLs
urllist2 = re.findall(r'([a-zA-Z0-9.\-*]+(\.com|\.net))',urlstring2)

# Extract full match URL results
for i in urllist2:
	urllist3.append(i[0])

# Sort and order
urllist4 = set(urllist3)
urllist5 = sorted(urllist4)


# Open output file and write output in correct format
with open('URL_IMPORT.txt', 'w+', newline='') as openfile:
	openfile.write('[Microsoft_URLs]\n')
	for i in urllist5:
		openfile.write(i + '\n')
