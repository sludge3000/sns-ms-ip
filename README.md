# SNS-MS-IP

Python script to retrieve all IPv4, IPv6 and URL addresses from https://docs.microsoft.com/en-us/office365/enterprise/urls-and-ip-address-ranges and output them as an importable CSV for the Stormshield Network Security appliance.

Written for Python 3.

Instructions:

Run this script on a machine with internet access.

The script will output two files.

IMPORT_ME.csv - IP address objects to import in the graphical users interface

URL_IMPORT.txt - URL objects in group to be copy pasted into urlgroup and cngroup configuration files.


sludge3000